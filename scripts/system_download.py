import argparse
import urllib
import sys
from pymongo import MongoClient
import os

# Parse the arguments
parser = argparse.ArgumentParser(description='Download systems and put them into MongoDb')
parser.add_argument('url', type=str)
parser.add_argument('--overwrite', '-o', type=str, help='Overwrites all the entries in Mongo, removing added data. DO NOT do this if you don\'t know what you\'re doing')
args = parser.parse_args()

temporary_file_name = 'temp.csv'
csvFile = '/mapSolarSystems.csv'

# Download the file
print 'Downloading file...',
try:
    file = urllib.URLopener()
    file.retrieve(args.url + csvFile, temporary_file_name)
except IOError:
    print 'Could not find a csv file at \'%s\'. Is the URL correct?' % (args.url + csvFile)
    sys.exit(-1)
print 'Done'

# Assemble list of systems from file
print 'Compiling System Objects...',
region_id = 0
constellation_id = 1
system_id = 2
system_name = 3
system_security = 21
system_entries = []

with open(temporary_file_name) as csv_file:
    content = csv_file.readlines()[1:]
    for line in content:
        tokens = line.split(',')
        system_entries.append({
            'name': tokens[system_name],
            'id'  : int(tokens[system_id]),
            'sec' : float(tokens[system_security])
        })
print 'Done'

# Insert into mongoDb
client = MongoClient('mongodb://localhost:27017/')
db = client.ids
systems = db.systems

if args.overwrite != None:
    systems.drop()

print 'Inserting System Objects...',
systems.insert_many(system_entries)
print 'Done\nDeleting Remaining Files.'

os.remove(temporary_file_name)
