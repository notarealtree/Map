'use strict';

var whmapApp = angular.module('whmapApp', [
    'ngRoute',
    'whmapControllers',
    'whmapServices'
]);

whmapApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/admin', {
            templateUrl: 'templates/admin.html',
            controller: 'AdminCtrl',
            currentTab: 'admin'
        }).
        when('/alerts', {
            templateUrl: 'templates/alerts.html',
            controller: 'AlertCtrl',
            currentTab: 'alerts'
        }).
        when('/login', {
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl',
            currentTab: 'login'
        }).
        when('/map', {
            templateUrl: 'templates/map.html',
            controller: 'MapCtrl',
            currentTab: 'map'
        }).
        when('/systems', {
            templateUrl: 'templates/systems.html',
            controller: 'SystemsCtrl',
            currentTab: 'systems'
        }).
        otherwise({
            redirectTo: '/'
        });
    }]);