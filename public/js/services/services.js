var whmapServices = angular.module('whmapServices', ['ngResource']);

whmapServices.service('systemService', ['$http', function($http){
    var base = 'http://localhost:8080';
    return{
        getAllSystems: getAllSystems,
        getSystem: getSystem
    };

    function getAllSystems(){
        console.log('running system req');
        return $http.get(base + '/system/list');
    }

    function getSystem(systemId){
        console.log('Running system request with', systemId);
        return $http.post(base + '/system/details', {id: systemId});
    }

}]);

whmapServices.service('alertService', ['$http', function($http){
    var base = 'http://localhost:8080';
    return {
        raiseAlert : raiseAlert
    };

    function raiseAlert(alert){
        console.log(alert);
        $http.post(base + '/alerts/raise', alert)
    }
}]);