/**
 * Created by Francis on 3/3/2016.
 */

var whmapControllers = angular.module('whmapControllers', []);

whmapControllers.controller('LoginCtrl', function($scope){

});

whmapControllers.controller('SystemsCtrl', ['$scope', 'systemService', function($scope, systemService){
    $scope.apiData = true;

    //TODO: Get these from eve api
    $scope.kills = 15;
    $scope.jumps = 121;
    $scope.rats = 3141;

    // TODO: Get these from our api
    $scope.signatures = ['CCP-123', 'XYZ-123'];

    $scope.anomalies = ['ABC-GTZ'];



    $scope.distances = ['Jita', 'Amarr', 'Dodixie', 'Rens'];

    $scope.selectSystem = function(system){
        system.isActive = true;
        systemService.getSystem(system).then(function(response){
            $scope.system = response.data[0];
        });
    };

    /**
     * Load the initial list of systems (simplified).
     */

    systemService.getAllSystems().then(function(response){
        console.log(response);
        $scope.systems = response.data;
    });

    $scope.getSystemName = function(){
        if($scope.system == undefined){
            return 'System Details';
        }
        return $scope.system.name;
    };

    // TODO move these into the factory/service
}]);

whmapControllers.controller('AdminCtrl', function($scope){

});

whmapControllers.controller('AlertCtrl', ['$scope', 'alertService', function($scope, alertService){
    //TODO: Move this into the database?
    $scope.alerts = [
        {
            colour: 'Blue'
        },{
            colour: 'Green'
        },{
            colour: 'Yellow'
        },{
            colour: 'Red'
        }
    ];

    $scope.alert = {};

    $scope.raise = function(){
        alertService.raiseAlert($scope.alert);
        $scope.alert = {};
    };

    $scope.activeAlerts = [
        {
            title: 'This sucks',
            raisedBy: 'Suckface',
            time: new Date(),
            expiry: new Date()
        }
    ]
}]);

whmapControllers.controller('MapCtrl', function($scope){

});

whmapControllers.controller('NavbarCtrl', function($scope, $route){
    $scope.$route = $route;
    $scope.isAdmin = true; //TODO: This only hides, admin functionality should be restricted otherwise as well.

});