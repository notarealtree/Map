var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var morgan     = require('morgan');
var cors       = require('cors');


app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(cors());

var port = process.env.PORT || 8080;
var router = express.Router();

require('./server/api/api')(app);

app.listen(port);
console.log('Api running on port ' + port);
