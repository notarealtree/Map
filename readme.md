
# Things you need

- NPM
- NodeJS
- MongoDB
- Grunt?

## To build

To make sure dependencies are up to date run the following commands (depending on which end you're working on).

### Frontend

Before doing anything else, run `npm install` and `bower_install`.

In order to then do frontend development:
- Run the api `node server.js`
- Run the development script `grunt develop` (leave this running)
- Run the grunt server `grunt serve`

### Backend

`npm install`

### Backend

`node server.js`

## Scripts

To download and fill the systems, usually run: `python system_download.py https://www.fuzzwork.co.uk/dump/yc118-2-116998/new/ --overwrite True`