module.exports = function(grunt) {

    // Imports
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-less');

    // Task configuration
    grunt.initConfig({
        clean: ['dist', 'public/styles/app.css'],
        concat: {
            js: {
                src: 'public/js/**/*.js',
                dest: 'dist/scripts/app.js'
            }
        },
        copy: {
            main: {
                files: [
                    {expand: true, cwd: 'public', src: [
                        'templates/**',
                        'components/**',
                        'bower_components/**',
                        'index.html'
                    ], dest: 'dist/'}
                ]
            }
        },
        uglify: {
            options: {
                mangle: false // <- Renames variables and stuff
            },
            js: {
                files: {
                    'dist/scripts/app.min.js': ['dist/scripts/app.js']
                }
            }
        },
        watch: {
            files: ['public/**/*'],
            tasks: ['build']
        },
        connect: {
            server: {
                options: {
                    port: 8000,
                    base: 'dist',
                    keepalive: true
                }
            }
        },
        less: {
            build: {
                files: {
                    'dist/styles/app.css': 'public/styles/main.less'
                }
            }
        }
    });

    // Tasks
    grunt.registerTask('build', ['clean', 'concat', 'less', 'copy', 'uglify']);
    grunt.registerTask('develop', ['build', 'watch']);
    grunt.registerTask('display', ['build', 'connect']);
    grunt.registerTask('serve', ['connect']);


};