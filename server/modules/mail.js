/**
 * Created by Francis on 3/19/2016.
 */

var nodemailer = require('nodemailer');
var config = require('../config');
var logger = require('./log');

var exports = module.exports = {};

port = nodemailer.createTransport(
    'smtps://' + config.mail.username +
    '%40' + config.mail.provider +
    ':' + config.mail.password + '@' +
    config.mail.smtpServer
);

var mailOptions = {
    from: config.mail.name + ' <' + config.mail.address + '>',
    to: config.mail.recipients
};

/**
 * Send email message with the parameters given in the config file
 * @param alert
 */
exports.sendMessage = function(alert){
    mailOptions.subject = alert.title;
    mailOptions.text = alert.text;

    transport.sendMail(mailOptions, function(error){
        if(error){
            logger.warn('E-Mail message for alert ' + alert.id + ' not sent: ' + error);
        }else{
            logger.info('E-Mail message for alert ' + alert.id + ' sent');
        }
        transport.close(); // shut down the connection pool, no more messages
    });
};

