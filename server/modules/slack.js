/**
 * Created by Francis on 3/19/2016.
 */

var exports = module.exports = {};

var config = require('../config');
var logger = require('./log');

var SlackBot = require('slackbots');

var bot = new SlackBot({
    token: config.slack.token,
    name: config.slack.name
});

bot.on('start', function(){
    logger.info('Slack Connection Opened');
});

exports.sendMessage = function(alert){
    var body = '*' + alert.title + '*\n\n' + alert.text;
    var params = {
        username    : config.slack.botName,
        icon_url    : config.slack.iconUrl,
        attachments : [{
            text        : body,
            color       : convertColor(alert.type),
            mrkdwn_in   : [
                'text',
                'pretext'
            ]
        }]
    };
    bot.postMessageToChannel(config.slack.channel, '<!channel>', params, function(){

    });
};

function convertColor(color){
    //TODO: Insert custom alert code here
    return color;
}
