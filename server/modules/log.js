/**
 * Created by Francis on 3/19/2016.
 */

var winston = require('winston');

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({ filename: 'api.log' })
    ]
});

module.exports = logger;