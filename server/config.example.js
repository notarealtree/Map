/**
 * Created by Francis on 3/19/2016.
 * Copy this file as config.js and fill in correct variable values.
 */


var config = {};

config.mongo = {};
config.slack = {};
config.mail = {};

config.mongo.address = 'localhost:27017';

config.slack.token = 'xoxb-...';
config.slack.botName = 'AlertBot';
config.slack.channel = 'alerts';
config.slack.iconUrl = '';

config.mail.address = 'example@gmail.com';
config.mail.username = 'example';
config.mail.provider = 'gmail.com';
config.mail.smtpServer = 'smtp.gmail.com';
config.mail.password = 'Password1234';
config.mail.recipients = ['example2@gmail.com'];

module.exports = config;