/**
 * Created by Francis on 2/6/2016.
 */

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/ids', function(){
    logger.info('Successfully connected to mongo');
});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var uuid = require('uuid');

var System = require('../models/systems');
var Alert = require('../models/alerts');

var slack = require('../modules/slack');
var mail = require('../modules/mail');
var logger = require('../modules/log');

module.exports = function(app){

    // TODO: Make these calls restful

    var list = []; //TODO: This is hacked while we lack redis

    /**
     * Returns status if the api is running, generally used to check if it is running.
     */
    app.get('/status', function(req,res){
        res.json('Map app is running!')
    });

    /**
     * Returns the specified fields for all systems in the database.
     */
    app.get('/system/list', function(req, res){
        logger.info('Getting System list');
        //noinspection JSUnresolvedFunction
        if(list.length > 0){
            res.json(list);
            return;
        }
        System.find({}, 'name sec id -_id', function(err, results){
            res.json(results);
            list = results;
        });
    });

    /**
     * Returns all fields for a specific system in the database.
     */
    app.post('/system/details', function(req, res){
        logger.info('Getting system details for ', req.body.id);
        //noinspection JSUnresolvedFunction
        System.find({id: req.body.id}, '-_id', function(err, result){
            res.json(result);
        })
    });

    /**
     * Adds an alert to the database and provides the source for slack/email calls
     */
    app.post('/alerts/raise', function(req, res){
        logger.info('Received new alert');
        var alert = new Alert({
            id      : uuid.v4(),
            time    : Date.now() / 1000,
            type    : req.body.color,
            title   : req.body.title,
            text    : req.body.body,
            expiry  : req.body.expiry,
            raisedBy: ''
        });
        alert.save(function(err){
            if(err){
                logger.warn('Failed to save alert to database: ', err);
                res.status(500).end();
            }else{
                res.status(200).end();
            }
        });
        // TODO: Make these configurable at some point
        slack.sendMessage(alert);
        mail.sendMessage(alert);
    });

};


