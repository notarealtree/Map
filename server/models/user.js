/**
 * Created by Francis on 2/29/2016.
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var userSchema = new Schema({
    userId  : String,
    email   : String,
    admin   : Boolean,
    enabled : Boolean,
    created : Number,
    alerts  : {
        green   : Boolean,
        yellow  : Boolean,
        red     : Boolean
    }
}, {collection: 'users'});

module.exports = mongoose.model('User', userSchema);