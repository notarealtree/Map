/**
 * Created by Francis on 3/2/2016.
 */


var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var mapSystemSchema = new Schema({
    name        : String,
    systemId    : Number,
    customName  : String,
    connections : [
        {
            type        : String,
            found       : Number,
            signature   : String,
            target      : Number
        }
    ]
}, {collection: 'map'});

module.exports = mongoose.model('MapSystem', mapSystemSchema);