/**
 * Created by Francis on 2/29/2016.
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var credentialsSchema = new Schema({
    userId  : String,
    username: String,
    ssoToken: String
}, {collection: 'credentials'});

module.exports = mongoose.model('Credentials', credentialsSchema);