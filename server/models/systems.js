/**
 * Created by Francis on 2/29/2016.
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var systemsSchema = new Schema({
    name        : String,
    id          : Number,
    sec         : Number,
    notes       : String,
    lastVisited : Number,
    towers      : [
        {
            owner   : String,
            active  : Boolean,
            planet  : Number,
            moon    : Number,
            modules : [
                {
                    id      : Number,
                    count   : Number
                }
            ]
        }
    ]
}, {collection: 'systems'});

module.exports = mongoose.model('System', systemsSchema);