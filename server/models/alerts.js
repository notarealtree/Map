var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var alertSchema = new Schema({
    id      : String,
    time    : Number,
    type    : String,
    raisedBy: String,
    title   : String,
    text    : String,
    expiry  : Number
}, {collection: 'alerts'});

module.exports = mongoose.model('Alert', alertSchema);